package booking;

import java.util.*;
import javax.jws.*;

import booking.business.BookingServiceImpl;

@WebService
public class BookingService {
	
	BookingServiceImpl bookingService = new BookingServiceImpl();
	
	@WebMethod
	public List<String> getList() {
		return bookingService.getList();
	}

	@WebMethod
	public String addBooking(
			@WebParam(name="passengerName") String passangerName, 
			@WebParam(name="departure") String departure, 
			@WebParam(name="arrival") String arrival, 
			@WebParam(name="from") String from, 
			@WebParam(name="to") String to) {
		return bookingService.addBooking(passangerName, departure, arrival, from, to);
	}
	
	@WebMethod
	public String removeBooking(@WebParam(name="bookingId") Integer id) {
		return bookingService.removeBooking(id);
	}
	
	@WebMethod
	public String updateBooking(
			@WebParam(name="bookingId") Integer id,
			@WebParam(name="passengerName") String passangerName, 
			@WebParam(name="departure") String departure, 
			@WebParam(name="arrival") String arrival, 
			@WebParam(name="from") String from, 
			@WebParam(name="to") String to) {
		return bookingService.updateBooking(id, passangerName, departure, arrival, from, to);
	}
}
