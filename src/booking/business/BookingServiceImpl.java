package booking.business;

import java.util.List;

public class BookingServiceImpl {
	
	private DB db = DB.getInstance();
	
	public BookingServiceImpl() {

		Booking a = new Booking(0, "Jan Novak", "05:00", "07:00", "Prague", "Rome");
		Booking b = new Booking(1, "Anna Dvorakova", "02:00", "06:00", "Prague", "Oslo");
		Booking c = new Booking(2, "Petr Svoboda", "10:00", "11:00", "Berlin", "Vienna");
		
		db.addBooking(a);
		db.addBooking(b);
		db.addBooking(c);

	}
	
	public List<String> getList() {
		return db.getAllBookings();
	}
	
	public String addBooking(String passangerName, String departure, String arrival, String from, String to) {
		int id = db.getNextId();
		Booking newBooking = new Booking(id, passangerName, departure, arrival, from, to);
		db.addBooking(newBooking);
		
		return "Booking added.";
	}
	
	public String updateBooking(Integer id, String passengerName, String departure, String arrival, String from, String to) {
		Booking updatedBooking = db.getBooking(id);
		updatedBooking.setPassangerName(passengerName);
		updatedBooking.setDeparture(departure);
		updatedBooking.setArrival(arrival);
		updatedBooking.setFrom(from);
		updatedBooking.setTo(to);
		
		return "Booking " + id + " updated.";
	}
	
	public String removeBooking(int id) {
		db.deleteBooking(id);
		return "Booking " + id + " deleted.";
	}
}
