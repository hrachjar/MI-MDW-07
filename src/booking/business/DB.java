package booking.business;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class DB {
    private static DB instance = null;
    private Hashtable<Integer, Booking> bookings = new Hashtable<Integer, Booking>();
    List<String> bookingList = new ArrayList<>();
 
    public static DB getInstance() {
        if (instance == null)
            instance = new DB();
        return instance;
    }
    
    public void addBooking(Booking b){
        bookings.put(b.getId(), b);
    }
    
    public Booking getBooking(int id) {
        return bookings.get(id);
    }
    
    public List<String> getAllBookings() {
    	bookingList.clear();
    	for (int id : bookings.keySet()) {
    		bookingList.add(bookings.get(id).toString());
  		}
    	return bookingList;
    }
    
    public void deleteBooking(int id) {
    	bookings.remove(id);
    }
    
    public int getNextId() {
    	return bookings.size();
    }
    
}