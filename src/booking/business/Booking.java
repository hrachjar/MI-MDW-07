package booking.business;

public class Booking {
	private int id;
	private String passangerName;
	private String departure;
	private String arrival;
	private String from;
	private String to;
	
	public Booking(int id, String passangerName, String departure, String arrival, String from, String to) {
		this.id = id;
		this.passangerName = passangerName;
		this.departure = departure;
		this.arrival = arrival;
		this.from = from;
		this.to = to;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPassangerName() {
		return passangerName;
	}

	public void setPassangerName(String passengerName) {
		this.passangerName = passengerName;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		this.arrival = arrival;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}
	
	@Override
	public String toString() {
		return "ID: " + id + ", name: " + passangerName + ", departure: " + departure + ", arrival: " + arrival
				+ ", from: " + from + ", to: " + to;
	}
	
}
